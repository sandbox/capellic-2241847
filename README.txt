DESCRIPTION

Extends Acquia Purge to allow for placing a block on a page for "purge this page" 
functionality. Addresses the situation of when you want to grant permissions to a content
edit who doesn't have administrative privileges so that they can purge the Varnish cache of
any given page if they notice any content (node, block, views) is stale.

DEPENDENCIES

Dependent on versions of Acquia Purge with the acquia_purge_purge_path() 
function, introduced in 1.0-beta2.

INSTALLATION

1. Install the module like usual (https://drupal.org/documentation/install/modules-themes/modules-7).

2. Set the permissions for 'Execute purge'.  Anonymous can't be used due to restrictions
in Acquia Purge that are sensible.

3. Add the block to a region in your theme. You should restrict the blocks' visibility to 
scope so that it does not appear in on admin/ paths.

4. Go to a page you want to purge, click on the "Purge This Page" button.  You will see 
that the page was purged by looking at your Watchdog log.

SPONSOR

Sponsored by Capellic, http:://www.capellic.com
@capellic
